import { Injectable } from '@angular/core';
import { FirebaseRepository } from '../repositories/firebase-repository';
import { Alumno } from '../models/alumno.model';
import { Observable, map } from 'rxjs';
import { ClasesService } from './clases.service';

@Injectable({
  providedIn: 'root'
})
export class AlumnosService {

  constructor(private clasesService: ClasesService, private firebaseRepository: FirebaseRepository) { }

  getAlumnosByDiaAndClase(diaId: string, claseId: string): Observable<Array<Alumno>> {
    return this.clasesService.getClasesByDia(diaId).pipe(
      map(clases => clases.find(clase => clase.id === claseId).alumnos)
    );
  }

  addAlumnoToClase(diaId: string, claseId: string, alumno: Alumno) {
    this.firebaseRepository.addAlumnoToClase(diaId, claseId, alumno);
  }

  deleteAlumno(diaId: string, claseId: string, alumno: Alumno) {
    this.firebaseRepository.deleteAlumno(diaId, claseId, alumno);
  }
}
