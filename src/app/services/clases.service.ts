import { Injectable } from '@angular/core';
import { FirebaseRepository } from '../repositories/firebase-repository';
import { Observable, map } from 'rxjs';
import { Clase } from '../models/clases.model';

@Injectable({
  providedIn: 'root'
})
export class ClasesService {

  constructor(private firebaseRepository: FirebaseRepository) { }

  getClasesByDia(diaId: string): Observable<Array<Clase>> {
    return this.firebaseRepository.getClasesByDia(diaId).pipe(
      map(clases => {
        return clases.sort((a, b) => this.sortClasesPorHorario(a, b));
      })
    );
  }

  addClaseToDia(diaId: string, clase: Clase) {
    this.firebaseRepository.addClaseToDia(diaId, clase)
  }

  deleteClaseFromDia(diaId: string, claseId: string) {
    this.firebaseRepository.deleteClaseFromDia(diaId, claseId);
  }

  private sortClasesPorHorario(a: Clase, b: Clase): number {
    if (a.horario > b.horario) {
      return 1;
    } 
    else if (a.horario < b.horario) {
      return -1;
    } 
    else {
      return 0;
    }
  }
}
