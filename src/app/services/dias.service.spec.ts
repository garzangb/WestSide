import { TestBed } from '@angular/core/testing';

import { DiasService } from './dias.service';
import { FirebaseRepository } from '../repositories/firebase-repository';

describe('DiasService', () => {
  let service: DiasService;
  let firebaseRepository: jasmine.SpyObj<FirebaseRepository>;

  beforeEach(() => {
    firebaseRepository = jasmine.createSpyObj<FirebaseRepository>('firebaseRepository', ['getDias']);

    TestBed.configureTestingModule({
      providers: [{ provide: FirebaseRepository, useValue: firebaseRepository}]
    });
    service = TestBed.inject(DiasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
