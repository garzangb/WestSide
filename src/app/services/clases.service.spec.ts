import { TestBed } from '@angular/core/testing';
import { ClasesService } from './clases.service';
import { FirebaseRepository } from '../repositories/firebase-repository';
import { of } from 'rxjs';

describe('ClasesService', () => {
  let service: ClasesService;
  let firebaseRepository: jasmine.SpyObj<FirebaseRepository>;

  beforeEach(() => {
    firebaseRepository = jasmine.createSpyObj<FirebaseRepository>('firebaseRepository', ['getClasesByDia']);

    TestBed.configureTestingModule({
      providers: [{ provide: FirebaseRepository, useValue: firebaseRepository}]
    });
    service = TestBed.inject(ClasesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('dada una lista de clases debería devolver la lista ordenada por horario', done => {
    firebaseRepository.getClasesByDia.and.returnValue(of([
      { horario: "14:00", tipo: "Calistenia", alumnos: [] },
      { horario: "10:00", tipo: "Calistenia", alumnos: [] },
    ]))

    const response = service.getClasesByDia("1");

    response.subscribe(clases => {
      expect(clases[0].horario).toBe("10:00");
      expect(clases[1].horario).toBe("14:00");
      done();
    });
  });
});
