import { Injectable } from '@angular/core';
import { FirebaseRepository } from '../repositories/firebase-repository';
import { Observable } from 'rxjs';
import { Dia } from '../models/dia.model';

@Injectable({
  providedIn: 'root'
})
export class DiasService {

  constructor(private firebaseRepository: FirebaseRepository) { }

  getDias(): Observable<Array<Dia>> {
    return this.firebaseRepository.getDias();
  }
}
