import { TestBed } from '@angular/core/testing';

import { AlumnosService } from './alumnos.service';
import { FirebaseRepository } from '../repositories/firebase-repository';

describe('AlumnosService', () => {
  let service: AlumnosService;
  let firebaseRepository: jasmine.SpyObj<FirebaseRepository>;

  beforeEach(() => {
    firebaseRepository = jasmine.createSpyObj<FirebaseRepository>('firebaseRepository', ['getClasesByDia']);

    TestBed.configureTestingModule({
      providers: [{ provide: FirebaseRepository, useValue: firebaseRepository }]
    });
    service = TestBed.inject(AlumnosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
