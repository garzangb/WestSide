import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AgregarClaseModalComponent } from './agregar-clase-modal.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

describe('AgregarClaseModalComponent', () => {
  let component: AgregarClaseModalComponent;
  let fixture: ComponentFixture<AgregarClaseModalComponent>;
  let activeModalSpy: jasmine.SpyObj<NgbActiveModal>;;  

  beforeEach(async () => {
    activeModalSpy = jasmine.createSpyObj<NgbActiveModal>('NgbActiveModal', ['close']);
    
    await TestBed.configureTestingModule({
      imports: [
        FormsModule
      ],
      declarations: [ AgregarClaseModalComponent ],
      providers: [{ provide: NgbActiveModal, useValue: activeModalSpy }]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AgregarClaseModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
