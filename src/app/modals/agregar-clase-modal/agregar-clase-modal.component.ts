import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Clase } from 'src/app/models/clases.model';

@Component({
  selector: 'gym-agregar-clase-modal',
  templateUrl: './agregar-clase-modal.component.html',
  styleUrls: ['./agregar-clase-modal.component.scss']
})
export class AgregarClaseModalComponent {

  @Input() dia: string = "";
  @Output() formSubmit: EventEmitter<Clase> = new EventEmitter<Clase>();

  public clase: Clase = { horario: "", tipo: "", alumnos: [] };
    
  constructor(public activeModal: NgbActiveModal) {}

  cerrar(): void {
    this.activeModal.close();
  }

  anotarse() {
    this.formSubmit.next(this.clase);
    this.activeModal.close();
  }

}
