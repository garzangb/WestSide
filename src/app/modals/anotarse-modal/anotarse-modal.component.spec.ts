import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AnotarseModalComponent } from './anotarse-modal.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

describe('AnotarseModalComponent', () => {
  let component: AnotarseModalComponent;
  let fixture: ComponentFixture<AnotarseModalComponent>;
  let activeModalSpy: jasmine.SpyObj<NgbActiveModal>;;  

  beforeEach(async () => {
    activeModalSpy = jasmine.createSpyObj<NgbActiveModal>('NgbActiveModal', ['close']);

    await TestBed.configureTestingModule({
      imports: [
        FormsModule
      ],
      declarations: [ AnotarseModalComponent ],
      providers: [ { provide: NgbActiveModal, useValue: activeModalSpy }]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AnotarseModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
