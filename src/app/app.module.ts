import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClassDetailsComponent } from './components/class-details/class-details.component';
import { AnotarseModalComponent } from './modals/anotarse-modal/anotarse-modal.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { initializeApp,provideFirebaseApp } from '@angular/fire/app';
import { environment } from '../environments/environment';
import { provideFirestore,getFirestore } from '@angular/fire/firestore';
import { AdminComponent } from './components/admin/admin.component';
import { AnotarseComponent } from './components/anotarse/anotarse.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AgregarClaseModalComponent } from './modals/agregar-clase-modal/agregar-clase-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    ClassDetailsComponent,
    AnotarseModalComponent,
    AdminComponent,
    AnotarseComponent,
    AgregarClaseModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule,
    provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideFirestore(() => getFirestore()),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
