import { Alumno } from "./alumno.model";

export interface Clase {
    id?: string;
    horario: string;
    tipo: string;
    alumnos: Array<Alumno>;
}