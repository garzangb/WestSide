import { Clase } from "./clases.model";

export class Dia {
    id?: string;
    nombre: string;
    clases?: Array<Clase>;
}