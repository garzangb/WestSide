import { Injectable } from "@angular/core";
import { Firestore, addDoc, arrayRemove, arrayUnion, collection, collectionData, deleteDoc, doc, updateDoc } from "@angular/fire/firestore";
import { Observable } from "rxjs";
import { Dia } from "../models/dia.model";
import { Clase } from "../models/clases.model";
import { Alumno } from "../models/alumno.model";

@Injectable({
    providedIn: 'root'
})
export class FirebaseRepository {

    constructor(private firestore: Firestore) { }

    getDias(): Observable<Array<Dia>> {
        const claseRef = collection(this.firestore, 'dias');
        return collectionData(claseRef, {idField: 'id'}) as Observable<Array<Dia>>
    }

    getClasesByDia(diaId: string): Observable<Array<Clase>> {
        const claseRef = collection(this.firestore, `dias/${diaId}/clases`);
        return collectionData(claseRef, {idField: 'id'}) as Observable<Array<Clase>>
    }

    addClaseToDia(diaId: string, clase: Clase) {
        const claseRef = collection(this.firestore, `dias/${diaId}/clases`);
        return addDoc(claseRef, clase);
    }

    addAlumnoToClase(diaId: string, claseId: string, alumno: Alumno) {
        const claseRef = doc(this.firestore, `dias/${diaId}/clases/${claseId}`);
        return updateDoc(claseRef, { alumnos: arrayUnion(alumno) } );
    }

    deleteAlumno(diaId: string, claseId: string, alumno: Alumno) {
        const claseRef = doc(this.firestore, `dias/${diaId}/clases/${claseId}`);
        return updateDoc(claseRef, { alumnos: arrayRemove(alumno) } );
    }

    deleteClaseFromDia(diaId: string, claseId: string) {
        const clasesRef = doc(this.firestore, `dias/${diaId}/clases/${claseId}`);
        return deleteDoc(clasesRef);
    }
}