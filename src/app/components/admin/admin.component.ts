import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AgregarClaseModalComponent } from 'src/app/modals/agregar-clase-modal/agregar-clase-modal.component';
import { Clase } from 'src/app/models/clases.model';
import { Dia } from 'src/app/models/dia.model';
import { ClasesService } from 'src/app/services/clases.service';
import { DiasService } from 'src/app/services/dias.service';

@Component({
  selector: 'gym-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  dias: Array<Dia> = [];
  panelOpenState: boolean = false;

  constructor(private modalService: NgbModal, private diasService: DiasService, private clasesService: ClasesService) {}

  ngOnInit(): void {
    this.diasService.getDias().subscribe(dias => {
      dias.forEach(dia => {
        this.clasesService.getClasesByDia(dia.id).subscribe(clases => {
          dia.clases = clases;
        })
      });

      this.dias = dias;
    });
  }

  agregarClase(dia: Dia) {
    const modalRef = this.modalService.open(AgregarClaseModalComponent, { centered: true, size: 'sm', backdrop: 'static', keyboard: false });
    modalRef.componentInstance.dia = dia.nombre;

    modalRef.componentInstance.formSubmit.subscribe((claseNueva: Clase) => {
      this.clasesService.addClaseToDia(dia.id, claseNueva);
    })
  }

  eliminarClase(diaId: string, claseId: string) {
    this.clasesService.deleteClaseFromDia(diaId, claseId);
  }
}
