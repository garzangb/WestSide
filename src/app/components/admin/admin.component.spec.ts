import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AdminComponent } from './admin.component';
import { of } from 'rxjs';
import { DiasService } from 'src/app/services/dias.service';
import { ClasesService } from 'src/app/services/clases.service';

describe('AdminComponent', () => {
  let component: AdminComponent;
  let fixture: ComponentFixture<AdminComponent>;
  let diasService: jasmine.SpyObj<DiasService>;
  let clasesService: jasmine.SpyObj<ClasesService>;
  
  beforeEach(async () => {
    diasService = jasmine.createSpyObj<DiasService>('DiasService', ['getDias']);
    clasesService = jasmine.createSpyObj<ClasesService>('ClasesService', ['getClasesByDia']);

    await TestBed.configureTestingModule({
      declarations: [ AdminComponent ],
      providers: [
        { provide: DiasService, useValue: diasService },
        { provide: ClasesService, useValue: clasesService },
      ]
    })
    .compileComponents();

    diasService.getDias.and.returnValue(of([]));

    fixture = TestBed.createComponent(AdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
