import { By } from '@angular/platform-browser';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AnotarseComponent } from './anotarse.component';
import { ClassDetailsComponent } from '../class-details/class-details.component';
import { Dia } from 'src/app/models/dia.model';
import { FirebaseRepository } from 'src/app/repositories/firebase-repository';
import { of } from 'rxjs';
import { ClasesService } from 'src/app/services/clases.service';

describe('AnotarseComponent', () => {

  let component: AnotarseComponent;
  let fixture: ComponentFixture<AnotarseComponent>;
  let clasesService: jasmine.SpyObj<ClasesService>;
  let firebaseRepository: jasmine.SpyObj<FirebaseRepository>;
  
  beforeEach(async () => {

    firebaseRepository = jasmine.createSpyObj<FirebaseRepository>('FirebaseRepository', ['getClasesByDia']);
    clasesService = jasmine.createSpyObj<ClasesService>('ClasesService', ['getClasesByDia']);

    await TestBed.configureTestingModule({
      declarations: [
        AnotarseComponent,
        ClassDetailsComponent
      ],
      providers: [ 
        { provide: ClasesService, useValue: clasesService },
        { provide: FirebaseRepository, useValue: firebaseRepository }
      ]
    }).compileComponents();

    clasesService.getClasesByDia.and.returnValue(of([]));
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnotarseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('debe retornar el Dia Lunes si el dia actual es lunes', () => {
    dadoDiasConClases();
    const fechaMock = new Date(2022, 5, 20); // lunes
    spyOn(Date, 'now').and.returnValue(fechaMock.getTime());

    component.ngOnInit();

    expect(component.diaSeleccionado.id).toBe("1");
    expect(component.diaSeleccionado.nombre).toBe("Lunes");
  });

  it('debe mostrar las clases del dia Lunes si el dia actual es lunes', () => {
    component.diaSeleccionado = lunes();
    fixture.detectChanges();

    const clases = fixture.debugElement.queryAll(By.css("gym-class-details"));

    expect(clases).toHaveSize(1);
  });

  function dadoDiasConClases() {
  }

  function lunes(): Dia {
    return { nombre: "Lunes", clases: [{ horario: "10:00", tipo: "Calistenia", alumnos: [] }]};
  }
});
