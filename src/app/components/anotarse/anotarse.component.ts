import { Component, OnInit } from '@angular/core';
import { Dia } from 'src/app/models/dia.model';
import { ClasesService } from 'src/app/services/clases.service';

@Component({
  selector: 'gym-anotarse',
  templateUrl: './anotarse.component.html',
  styleUrls: ['./anotarse.component.scss']
})
export class AnotarseComponent implements OnInit{

  public dias = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"];
  public diaSeleccionado: Dia = { nombre: "" };

  constructor(private clasesService: ClasesService) {}

  ngOnInit(): void {
    this.diaSeleccionado.id = this.getDiaDeSemana().toString();
    this.diaSeleccionado.nombre = this.dias[this.getDiaDeSemana() -1];

    this.clasesService.getClasesByDia(this.getDiaDeSemana().toString()).subscribe(clases => {
      this.diaSeleccionado.clases = clases;
    });
  }
  
  public getDiaDeSemana(): number {
    return new Date(Date.now()).getDay();
  }
}
