import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ClassDetailsComponent } from './class-details.component';
import { of } from 'rxjs';
import { AlumnosService } from 'src/app/services/alumnos.service';

describe('ClassDetailsComponent', () => {
  let component: ClassDetailsComponent;
  let fixture: ComponentFixture<ClassDetailsComponent>;
  let alumnosService: jasmine.SpyObj<AlumnosService>;
  
  beforeEach(async () => {

    alumnosService = jasmine.createSpyObj<AlumnosService>('alumnosService', ['getAlumnosByDiaAndClase', 'addAlumnoToClase']);

    await TestBed.configureTestingModule({
      declarations: [ ClassDetailsComponent ],
      providers: [
        { provide: AlumnosService, useValue: alumnosService},
      ]
    })
    .compileComponents();

    alumnosService.getAlumnosByDiaAndClase.and.returnValue(of([]));
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
