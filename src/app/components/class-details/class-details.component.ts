import { Component, Input, OnInit } from '@angular/core';
import { Clase } from 'src/app/models/clases.model';
import { AnotarseModalComponent } from 'src/app/modals/anotarse-modal/anotarse-modal.component';
import { Alumno } from 'src/app/models/alumno.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FirebaseRepository } from 'src/app/repositories/firebase-repository';
import { AlumnosService } from 'src/app/services/alumnos.service';

@Component({
  selector: 'gym-class-details',
  templateUrl: './class-details.component.html',
  styleUrls: ['./class-details.component.scss']
})
export class ClassDetailsComponent implements OnInit{

  @Input() clase: Clase = { horario: "", tipo: "", alumnos: [] };
  @Input() diaId: string;

  constructor(private modalService: NgbModal, private alumnosService: AlumnosService) {}

  ngOnInit(): void {
    this.alumnosService.getAlumnosByDiaAndClase(this.diaId, this.clase.id).subscribe(alumnos => {
      this.clase.alumnos = alumnos;
    });
  }

  anotarse() {
    const modalRef = this.modalService.open(AnotarseModalComponent, { centered: true, size: 'sm', backdrop : 'static', keyboard : false});
    modalRef.componentInstance.clase = `${this.clase.tipo} ${this.clase.horario}`;
    
    modalRef.componentInstance.formSubmit.subscribe((alumnoNuevo: any) => {
      this.alumnosService.addAlumnoToClase(this.diaId, this.clase.id, alumnoNuevo);
    })
  }

  desanotarme(alumno: Alumno) {
    this.alumnosService.deleteAlumno(this.diaId, this.clase.id, alumno)
  }
}
