// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'westside-cf028',
    appId: '1:933004593978:web:7e48437ff44e0a5c8e1e03',
    databaseURL: 'https://westside-cf028-default-rtdb.firebaseio.com',
    storageBucket: 'westside-cf028.appspot.com',
    apiKey: 'AIzaSyCvFL5Gg97tWaVP4jQ7fnOqvSpZC1kGE2w',
    authDomain: 'westside-cf028.firebaseapp.com',
    messagingSenderId: '933004593978',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
